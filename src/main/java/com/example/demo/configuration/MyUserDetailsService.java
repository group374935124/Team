package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;

import com.example.demo.repositories.UserRepo;
import com.example.demo.security.MyUserDetails;
import com.example.demo.models.User;

@Service
public class MyUserDetailsService implements UserDetailsService{
    //Dependency Injection
    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        //Load the user from JPA
        Optional<User> returnedUser = userRepo.findByUserName(username);
        User user = returnedUser.orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return new MyUserDetails(user);
    }
    
}
