package com.example.demo.services;

import com.example.demo.models.Team;
import com.example.demo.repositories.TeamRepo;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class TeamService {
    @Autowired
    private TeamRepo teamRepo;
    
    public Team newTeam(Team team){
        return teamRepo.save(team);
    }

    public List<Team> getTeamList(){
        return teamRepo.findAll();
    }
}
